(ns re-frame-sortable-table.examples
  (:require
    [re-frame.core :as re-frame]
    [re-frame-sortable-table.subs :as subs]
    [reagent.core :as reagent]))

(def data
  [["Name" "Weapon" "Side" "Height (m)"]
   ["Luke Skywalker" "Blaster" "Good" 1.72]
   ["Leia Organa" "Blaster" "Good" 1.5]
   ["Han Solo" "Blaster" "Good" 1.8]
   ["Obi-wan Kenobi" "Light Saber" "Good" 1.82]
   ["Chewbacca" "Bowcaster" "Good" 2.28]
   ["Darth Vader" "Light Saber" "Bad" 2.03]])

(re-frame/reg-sub
  :table
  (fn [db [_ key]]
    (get-in db [:tables key])))

(defn sortable-table [table-key]
  (let [s (reagent/atom {})]
    (fn [table-key]
      (let [table @(re-frame/subscribe [:table :new-hope])
            key (:sort-key @s)
            dir (:sort-direction @s)
            rows  (cond->> (:rows table)
                           key (sort-by #(nth % key))
                           (= :ascending dir) reverse)
            sorts [key dir]]
        [:table
         {:style {:font-size "150%"}}
         [:tbody
          [:tr
           (for [[i h] (map vector (range) (:header table))]
             [:th
              {:style    {:line-height "1em"}
               :on-click #(cond
                            (= [i :descending] sorts)
                            (swap! s assoc
                                   :sort-direction :ascending)
                            (= [i :ascending] sorts)
                            (swap! s dissoc
                                   :sort-key :sort-direction)
                            :else
                            (swap! s assoc
                                   :sort-key i
                                   :sort-direction :descending))}

              [:div {:style {:display        :inline-block
                             :vertical-align :middle}}
               h]
              [:div {:style {:display        :inline-block
                             :vertical-align :middle
                             :font-size      "60%"
                             :line-height    "1em"}}
               [:div
                {:style {:color (if (= [i :descending]
                                       sorts)
                                  :black
                                  "#aaa")}}
                "▲"]
               [:div
                {:style {:color (if (= [i :ascending]
                                       sorts)
                                  :black
                                  "#aaa")}}
                "▼"]]])]
          (for [row rows]
            [:tr
             (for [v row]
               [:td v])])]]))))

(defn ui []
  [:div
   [sortable-table :new-hope]])


