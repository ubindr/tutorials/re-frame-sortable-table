(ns re-frame-sortable-table.events
  (:require
   [re-frame.core :as re-frame]
   [re-frame-sortable-table.db :as db]))


(re-frame/reg-event-db
 ::initialize-db
 (fn [_ _]
   {:tables {:new-hope {:header (first db/data)
                        :rows (vec (rest db/data))}}}))

(re-frame/reg-event-db
 ::set-active-panel
 (fn [db [_ active-panel]]
   (assoc db :active-panel active-panel)))
